
(function(window, $){

	'use strict';
	
	// Document -------------------------------------------------------------------
	
	$(document).ready(function () {
	
		console.log('## Document ready');
	
		// VARS -------------------------------------------------------------------
		//
		var 	$document 					= $(document),
					$form 							= $('form'),
					$btnSubmit 					= $('#btnSubmit'),
					$numericInput 			= $('input[type=number]'),
					$telephoneCountryCodeInput = $('.field--telephone__code'),
					defaultCountryCode 	= '32',
					$telephoneInput 		= $('input[type=tel]'),
					$textInput 					= $('input[type=text], textarea'),
					$emailInput 				= $('input[type=email]'),
					$checkboxInput			= $('input[type=checkbox]'),
					$radioInput					= $('input[type=radio]'),
					$kboInput 					= $('.field--kbo input[type=number]'),
					isValid 						= true,
					$selectLabel 				= $('.label--select'),
					$selectOption 			= $('.label--select__list li'),
					$selectInput 				= $('.label--select input[type=text]'),
					$changeCampaignCode = $('.change-campaign-code input[type=checkbox], .change-campaign-code input[type=radio]'),
					$preventZero 				= $('.prevent-zero')
		;
	
		// ACTIONS ----------------------------------------------------------------
		//
	
		$form.on('submit', validateForm);
		$numericInput.on('keypress', validateNumericInput);
		$numericInput.on('keyup', validateMaxlength);
		$telephoneInput.on('keypress', validateNumericInput);
		$telephoneInput.on('keyup', validateTelInput);
		$telephoneInput.on('focus', focusField);
		$telephoneInput.on('blur', blurField);
		$telephoneCountryCodeInput.on('keyup', validateTelInput);
		$telephoneCountryCodeInput.on('blur', setToDefault);
		$kboInput.on('focus', focusField);
		$kboInput.on('blur', blurField);
		$kboInput.on('keyup', validateKboInput);
		$textInput.on('keypress', preventCertainInput);
		$textInput.on('keyup', validateTextInput);
		$emailInput.on('keypress', preventCertainInput);
		$emailInput.on('keyup', validateTextInput);
		$emailInput.on('blur', suggestEmailFormat);
		$selectLabel.on('click', openSelect);
		$selectOption.on('click', selectOption);
		$selectInput.on('focus', scrollToElement);
		$checkboxInput.on('change', checkboxInputChangeHandler);
		$radioInput.on('change', radioInputChangeHandler);
		$changeCampaignCode.on('change', changeCampaignCode);
		$preventZero.on('keyup', preventZeroKeyup)
	
		/**
		 * Prevent submitting double values on page load.
		 * Reproduce error: comment these lines and click on "back" arrow after submit.
		 */
		$.each($checkboxInput, function(){
			disableHiddenInput($(this));
		});
	
		//
		// FUNCTIONS --------------------------------------------------------------
		//
	
		/**
		 * Validate the form on submit.
		 */
		function validateForm(e) {
			var $form = $(this);
			isValid = true;
			$('#feedback').remove();
			$('.is-invalid').removeClass('is-invalid');
			
			validateRequiredFields($form);
			validateEmailAddresses();
			validatePhoneNumbers();
			validateKboNumbers();
	
			if (!isValid) {
				$form.addClass('is-active-validation');

				// Scroll to first invalid element
				$('.is-invalid').first().velocity('scroll', { duration: 250, mobileHA: false, offset: -20 });

				e.preventDefault();
				return false;
			} else {
				$btnSubmit.prop("disabled", !0).addClass("is-submitting").text("Submitting form...");
			}
		}
	
		/**
		 * Submit Validation Functions
		 * ===========================
		 */
	
		/**
		 * Check all required form fields for empty input.
		 */
		function validateRequiredFields(form) {
			var $requiredFields = $(form).find('input[required], textarea[required]');
			var $requiredGroups = $('.js-required');
			var $nonRequiredFields = $(form).find('input, textarea').not('input[required], textarea[required], .js-required input, input[type=hidden], input[readonly], input[disabled], .is-hidden input, .is-hidden textarea');
			
	
			$.each($requiredFields, function(){
				var $this = $(this);
				var $label = $this.closest('label, .label');
	
				if ($.trim($this.val()) === '' || ( $this.is('input[type=radio]') && $('input[name=' + $this.attr('name') + ']:checked').length === 0 ) || ($this.is('input[type=checkbox]') && $this.prop('checked') === false || $.trim($this.val()) === '0')) {
					isValid = false;
					markInvalidInput($label);
				} else {
					markValidInput($label);
				}
			});
	
			$.each($requiredGroups, function(){
				var $this = $(this);
	
				if ($this.find('input[type=checkbox]:checked').length === 0) {
					isValid = false;
					markInvalidInput($this);
				} else {
					markValidInput($this);
				}
			});
	
			$.each($nonRequiredFields, function() {
				markValidInput($(this).closest('label, .label'));
			});
		}
	
		/**
		 * Validate the format of one or multiple email addresses in the form.
		 */
		function validateEmailAddresses() {
			$.each($emailInput, function() {
				if ((!isValidEmailAddress($(this).val()) && $(this).prop('required') == true) || (!isValidEmailAddress($(this).val()) && $(this).prop('required') == false && $(this).val().length !== 0)) {
					markInvalidInput($(this).closest('label, .label'));
				}
			});
		}

		/**
		 * Validate the format of telephone numbers.
		 */
		function validatePhoneNumbers() {
			$.each($telephoneInput, function() {
				var $this = $(this);
				var isRequired = $this.prop('required');
				var value = $this.val();
	
				if (isRequired && value.length < 9) {
					markInvalidInput($(this).closest('label, .label'));
				}
			});
		}

		/**
		 * Validate the format of kbo numbers.
		 */
		function validateKboNumbers() {
			$.each($kboInput, function() {
				var $this = $(this);
				var isRequired = $this.prop('required');
				var value = $this.val();
	
				if (isRequired && value.length < 9) {
					markInvalidInput($(this).closest('label, .label'));
				}
			});
		}
	
		/**
		 * Live Validation Functions
		 * ===========================
		 */
	
		/**
		 * Handle all live validation related to numeric input.
		 */
		function validateNumericInput(e) {
			var $this = $(this);
			var value = $this.val();

			preventZeroKeypress($this, e);
	
			return e.metaKey || // cmd/ctrl
			e.which <= 0 || // arrow keys
			e.which == 8 || // delete key
			/[0-9]/.test(String.fromCharCode(e.which));
		}

		/**
		 * Handle all live validation related to textual input.
		 */
		function validateTextInput(e) {
			var $this = $(this);
			var $label = $this.closest('label, .label');
			var value = $.trim($this.val());
			var isRequired = $this.prop('required');
	
			// Prevent tab key from triggering invalid state.
			if (e.which == 9) { return false; }
	
			// If the field is required and the value is empty on keyup, mark the field as invalid
			if (isRequired === true && value.length === 0) {
				markInvalidInput($label);
				return false;
			}
	
			if ($this.prop('tagName') === 'TEXTAREA' && $label.find('.charcounter span').length > 0) {
				$label.find('.charcounter span').text($this.val().length);
			}
	
			if (($this.attr('type') === 'email' && !isValidEmailAddress(value) && isRequired) || $this.attr('type') === 'email' && !isValidEmailAddress(value) && !isRequired && value.length !== 0) {
				markInvalidInput($label);
				return false;
			}
	
			// Mark valid by default
			markValidInput($label);
		}
	
		/**
		 * Handle all live validation related to telephone numbers.
		 */
		function validateTelInput() {
			var $this = $(this);
			var $label = $this.closest('label, .label');
			var value = $this.parent().find('input[type=tel]').val();
			var valueCountryCode = $this.parent().find('.field--telephone__code').val();
			var isRequired = $this.parent().find('input[type=tel]').prop('required');

			// Prevent leading zeros in input.
			if (!isNaN(parseInt($this.val()))) {
				$this.val(parseInt($this.val(), 10));
			}
			
			// Mark valid when 9 tel numbers and at least 1 countrycode number
			if ((value.length === 9 && valueCountryCode.length > 0) || !isRequired && value.length === 0) {
				markValidInput($label);
			} else if (value.length !== 0) {
				markInvalidInput($label);
			}
	
			if (value.length !== 0) {
				value = '+' + valueCountryCode + value;
			} else {
				value = '';
			}
	
			$this.siblings('input[type=hidden]').val(value);
		}
	
		/**
		 * Handle all live validation related to kbo numbers.
		 */
		function validateKboInput(e) {
			var $this = $(this);
			var value = $this.val();
			var $label = $this.closest('label, .label');
			var isRequired = $this.prop('required');
			var $warning = $label.find('.info--warning');
			var $warningLoading = $label.find('.info--warning-loading');
			var $kboExists = $label.find('.kbo-exists');

			$warning.addClass('is-hidden');
			$warningLoading.addClass('is-hidden').removeClass('is-submitting');
			$label.removeClass('is-warning');

			// Prevent leading zeros in input.
			if (!isNaN(parseInt($this.val()))) {
				$this.val(parseInt($this.val(), 10));
			}
	
			if ((isRequired && value.length < 9) || (!isRequired && value.length > 0 && value.length < 9)) {
				markInvalidInput($label);
			} else {
				markValidInput($label);
			}
	
			if (value.length !== 0) {
				value = 'BE0' + value;
			} else {
				value = '';
			}

			if (value.length === 12) {
				_elqQ.push(['elqDataLookup', escape('4381b9b296aa4d2ea44dcc11e3eb8b4e'),'<KBO_Number1>' + value + '</KBO_Number1>']);

				// Show "Checking KBO-number..." message
				$warningLoading.removeClass('is-hidden').addClass('is-submitting');

				$label.find('.info:not(.info--warning):not(.info--warning-loading)').addClass('is-hidden');

				// Reset hidden checkbox
				$kboExists.prop('checked', false);

				// UNCOMMENT THIS IF NO LOADING MESSAGE (FOR FASTER FEEDBACK)
				// Do not show the warning message if a change on the checkbox has been detected
				// $kboExists.off('change').on('change', function(e) {
				// 	var $target = $(e.target);

				// 	if ($target.prop('checked') === true) {
				// 		$warning.addClass('is-hidden');
				// 		$label.removeClass('is-warning');
				// 		$warningLoading.addClass('is-hidden').removeClass('is-submitting');
				// 	}
				// });

				// If the hidden checkbox has not been checked in xxxx milliseconds, show the warning message.
				setTimeout(function() {
					if($kboExists.prop('checked') === false) {
						$label.addClass('is-warning');
						$warning.removeClass('is-hidden');
					} else {
						$label.removeClass('is-warning');
						$warning.addClass('is-hidden');
					}

					$warningLoading.addClass('is-hidden').removeClass('is-submitting');
				}, 2000);
			} else if (value.length !== 12) {
				$label.find('.info:not(.info--warning):not(.info--warning-loading)').removeClass('is-hidden');
			}
	
			// Pass the complete value of the KBO to a hidden input for submission.
			$label.find('.kbo-full').val(value);
		}
	
		/**
		 * Handle all live validation related to radiobuttons.
		 */
		function radioInputChangeHandler() {
			markValidInput($(this).closest('label'));
		}
	
		/**
		 * Handle all live validation related to checkboxes.
		 */
		function checkboxInputChangeHandler() {
			var $this = $(this);
			var isChecked = $this.prop('checked');
			var isRequired = $this.prop('required');
			var $requiredGroup = $this.parents('.js-required');
			var $label = $this.closest('label, .label');
	
			if (isRequired && !isChecked) {
			// Mark invalid if the checkbox is required and not checked.
				markInvalidInput($label);
			} else if ($requiredGroup.length > 0 && $requiredGroup.find('input[type=checkbox]:checked').length !== 0) {
			// Mark valid if the checkbox-group is required and at least 1 checkbox is checked.
				markValidInput($requiredGroup.prev('.label'));
			} else if ($requiredGroup.length > 0 && $requiredGroup.find('input[type=checkbox]:checked').length === 0) {
				// Mark invalid if the checkbox-group is required and no checkboxes are checked.
				markInvalidInput($requiredGroup.prev('.label'));
			} else {
				// Mark valid by default.
				markValidInput($label);
			}
	
			// Disable hidden inputs linked to checkboxes with the same name to prevent submitting a double value.
			disableHiddenInput($this);
		}

		/**
		 * Utility Functions
		 * ===========================
		 */
	
		/**
		 * Prevent the input of certain characters.
		 */
		function preventCertainInput(e) {
			var $this = $(this);
			var value = $.trim($this.val());
	
			// Prevent adding spaces before any other value in text fields or prevent spaces alltogether in email addresses.
			if ((e.which == 32 && value.length === 0) || ($this.attr('type') === 'email' && e.which == 32)) { e.preventDefault(); return false; }

			// Prevent starting with a 0 if element has .prevent-zero class.
			if ($this.hasClass('prevent-zero')) {
				preventZeroKeypress($this, e);
			}
		}

		/**
		 * Prevent starting a string or number with 0.
		 */
		function preventZeroKeypress(element, event) {
			var value = element.val();

			if (value.length === 0 && event.which == 48) {
				event.preventDefault();
			}
		}

		// Prevent leading zeros in input on keyup, in case whole input is deleted by select and press 0.
		function preventZeroKeyup() {
			var $this = $(this);
			var value = $this.val();
			var $label = $this.closest('label, .label');

			if (!isNaN(parseInt(value))) {
				$this.val(parseInt(value, 10));
			}

			if ($.trim($this.val()) === '0') {
				markInvalidInput($label);
			}
		}
	
		/**
		 * Check the maximum length of the input. Most useful for field type "number".
		 */
		function validateMaxlength(e) {
			var $this = $(this);
			var maxlength = parseInt($this.attr('maxlength'));
			var value = $this.val();

			// Prevent exceeding maxlength value for numeric fields
			if (value.length >= maxlength) {
				value = $this.val().slice(0, maxlength);
				$this.val(value);
			}
		}

	
		/**
		 * Other Functions
		 * ===========================
		 */
	
		 /**
		 * Handle selecting of an option in custom dropdown lists.
		 */
		function selectOption() {
			var $this = $(this);
			var text = $this.text();
			var value = $this.attr('data-value');
	
			$this.addClass('is-active').siblings().removeClass('is-active');
			$this.closest('.label--select__list').prev('input[type=text]').val(text);
			$this.closest('.label--select__list').next('input[type=text]').val(value);
	
			if ($this.closest('.label--select__list').next('input[type=text]').val().length === 0 && $this.closest('.label--select__list').next('input[type=text]').prop('required') === true) {
				$this.closest('.label--select').removeClass('is-valid').addClass('is-invalid');
			} else {
				$this.closest('.label--select').removeClass('is-invalid').addClass('is-valid');
			}
	
			checkDataBinds($this);
		}
	
		/**
		 * Open the dropdown list when clicking on the input-field or the associated label.
		 */
		function openSelect(e) {
			e.stopPropagation();
			e.preventDefault();

			// Remove all focused-classes except the current active one, to prevent multiple dropdown lists of being open at the same time.
			$('.is-focused').not($(this)).removeClass('is-focused');
	
			$(this).find('input[type=text]:first-of-type').focus();
	
			if ($(this).hasClass('is-focused')) {
				$(this).removeClass('is-focused');
				$(this).find('input[type=text]:first-of-type').blur();
				$document.off('.hideOpenSelects', hideOpenSelects);
			} else {
				$(this).addClass('is-focused');
				$document.on('click.hideOpenSelects', hideOpenSelects);
			}
		}
	
		/**
		 * Hide the dropdown list when clicking on the input-field or label again, or elsewhere in the document.
		 */
		function hideOpenSelects(e) {
			if (!$selectLabel.is(e.target) && $selectLabel.has(e.target).length === 0) {
				$selectLabel.removeClass('is-focused');
				$document.off('.hideOpenSelects', hideOpenSelects);
			} else if ( $selectLabel.is(e.target) ) {
				$document.off('.hideOpenSelects', hideOpenSelects);
			}
		}
	
		/**
		 * Handle bound elements - if any.
		 * @param {HTMLElement} element 
		 */
		function checkDataBinds(element) {

			if (element.attr('data-bind') && element.attr('data-bind-required') == 'true') {
				var $target = $('#' + element.attr('data-bind'));
	
				$target.removeClass('is-hidden').find('.asterisk').removeClass('is-hidden');
				$target.find('*[data-js-required=true]').addClass('js-required');
				$target.find('input:not([type=hidden])').not('.js-required input').prop('required', true);
			} else if (element.attr('data-bind') && element.attr('data-bind-required') == 'false') {
				var $target = $('#' + element.attr('data-bind'));
				$target.removeClass('is-hidden').find('.asterisk').removeClass('is-hidden');
				$target.find('.asterisk').addClass('is-hidden');
				$target.find('.js-required').removeClass('js-required');
				$target.find('input:not([type=hidden])').prop('required', false);
			} else {
				$.each(element.siblings(), function(){
					if ($(this).attr('data-bind')) {
						var $target = $('#' + $(this).attr('data-bind'));
						$target.addClass('is-hidden').find('.asterisk').addClass('is-hidden');
						$target.find('.is-invalid').removeClass('is-invalid');
						$target.find('.js-required').removeClass('js-required');
						$target.find('input:not([type=hidden])').not('.js-required input').prop('required', false);
					}
				});
			}
		}
	
		/**
		 * If there is a sibling hidden input with the same name as the checkbox, disable it when the checkbox is checked. This prevents submission of double values.
		 */
		function disableHiddenInput(element) {
			var $this = element;
			var name = $this.attr('name');
			
			if ($this.prop('checked') === true && $this.siblings('input[type=hidden][name=' + name + ']').length > 0) {
				$('input[type=hidden][name=' + name + ']').prop('disabled', true);
			} else {
				$('input[type=hidden][name=' + name + ']').prop('disabled', false);
			}
		}
	
		/**
		 * Mark invalid form fields.
		 */
		function markInvalidInput(formField) {
			if ($(formField).hasClass('radiobutton-wrapper')) {
				$(formField).parent().prev('.label').removeClass('is-valid').addClass('is-invalid');
			} else if ($(formField).hasClass('checkbox-group')) {
				$(formField).prev('.label').removeClass('is-valid').addClass('is-invalid');
			} else if ($(formField).closest('.checkbox-group').length > 0) {
				$(formField).parent().prev('.label').removeClass('is-valid').addClass('is-invalid');
			} else {
				$(formField).removeClass('is-valid').addClass('is-invalid');
			}
		}
	
		/**
		 * Mark valid form fields.
		 */
		function markValidInput(formField) {
			if ($(formField).hasClass('radiobutton-wrapper')) {
				// Mark valid specifically for radiobuttons
				$(formField).parent().prev('.label').removeClass('is-invalid').addClass('is-valid');
			}	else if ($(formField).hasClass('checkbox-group')) {
				// Mark valid specifically for checkbox groups
				$(formField).prev('.label').removeClass('is-invalid').addClass('is-valid');
			} else if ($(formField).closest('.checkbox-group').length > 0) {
				// Mark valid specifically for children of checkbox groups
				$(formField).parent().prev('.label').removeClass('is-invalid').addClass('is-valid');
			} else {
				// Mark valid by default
				$(formField).removeClass('is-invalid').addClass('is-valid');
			}
		}
	
		/**
		 * Check validity of email address
		 */
		function isValidEmailAddress(emailAddress) {
			return emailAddress.length > 3 && emailAddress.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i);
		}
	
		/**
		 * Add a focus class to the parent of an input field.
		 */
		function focusField() {
			$(this).parent().addClass('is-focused');
		}
	
		/**
		 * Remove the focus class on the parent of an input field.
		 */
		function blurField() {
			$(this).parent().removeClass('is-focused');
		}

		/**
		 * Reset Telephone Country Code to default value when left empty.
		 */
		function setToDefault() {
			var $this = $(this);
			var value = $this.val();
			var valueTelephone = $this.parent().find('.field--telephone__code').next('input').val();
	
			if (value.length === 0) {
				$this.val(defaultCountryCode);
				$this.siblings('input[type=hidden]').val('+' + $this.val() + valueTelephone);
			}
		}

		// Scroll to the currently focused element. Disable Mobile Hardware Acceleration on VelocityJS to prevent z-index flickering on mobile.
		function scrollToElement() {
			var $this = $(this);

			// Add tiny delay to prevent scroll from not opening dropdown list.
			window.setTimeout(function() {
				$this.parent().velocity('scroll', { duration: 250, mobileHA: false, offset: -20 });
			}, 50);
		}

		/**
		 * Suggest correction if the user made a small type in his/her email address.
		 */
		function suggestEmailFormat() {
			var $this = $(this);
			var $label = $this.closest('label, .label');

			$this.mailcheck({
				suggested: function(element, suggestion) {
					var $warning = $(element).closest('label').find('.info--warning');

					$warning.removeClass('is-hidden').find('a').text(suggestion['full']);
					$warning.find('a').off('click').on('click', function(e) {
						e.preventDefault();
						$this.val(suggestion['full']);
						$warning.addClass('is-hidden');
						$label.removeClass('is-warning');
					});

					$label.addClass('is-warning');
				},
				empty: function(element) {
					var $warning = $(element).closest('label').find('.info--warning');
					$warning.addClass('is-hidden');
					$label.removeClass('is-warning');
				}
			});
		}

		/**
		 * Select the correct campaign codes for processing in SIEBEL.
		 */
		function changeCampaignCode() {
			var $this = $(this);
			var $group = $this.closest('.change-campaign-code');
			var $inputs = $group.find('input[type=checkbox], input[type=radio]');
			var active = [];
			
			// Loop over all inputs that can affect the campaign code.
			$.each($inputs, function(index, element) {
				var $element = $(element);
				var isChecked = $element.prop('checked');
				var code = $element.attr('data-code');
				var campaign = '#' + $element.attr('data-campaign');

				// Reset values
				$(campaign).val('');

				// If a value is checked and unique, push it to an active array.
				if (isChecked) {
					var found = active.some(function(el) {
						return el.campaign === campaign && el.code === code;
					});
					
					if(!found) {
						active.push({
							campaign: campaign,
							code: code
						});
					}
				}
			});

			// For each value in the active array, fill in the campaign code in the hidden inputs.
			if (active.length > 0) {
				for (var i = 0; i < active.length; i++) {
					$(active[i].campaign).val(active[i].code);
				}
			}
		}
	
		/**
		 * Search the URL for parameters when necessary
		 */
		function getParameterByName(name, url) {
			if (!url) { url = window.location.href; }
			name = name.replace(/[\[\]]/g, '\\$&');
			var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
				results = regex.exec(url);
			if (!results) { return null; }
			if (!results[2]) { return ''; }
			return decodeURIComponent(results[2].replace(/\+/g, ' ').replace(/,|;/g, ''));
		}
	});
	
	}(window, jQuery));

	/**
	 * This function triggers when an Eloqua web data lookup returns a value.
	 */
	function SetElqContent() {
		var kbo = GetElqContentPersonalizationValue('KBO_Number1');

		if (kbo.length > 0) {
			$('.kbo-exists').prop('checked', true).change();
		}
	}